/*
Copyright (C) 2018 Matthew Leon

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "file_ops.h"

int rom_file;

/* This function just gets the file name of the rom and returns it */
char* get_rom_file_name(void)
{
	int ret_val = -1;

	char* rom_file_name = (char*)malloc(4096);
	if(NULL == rom_file_name) {
		perror("malloc in get_rom_file_name");
		return rom_file_name;
	}

	/*Read file name in*/
	printf("Please give file name to run/disassemble\n");
	ret_val = scanf("%4096s", rom_file_name);	
	if(1 != ret_val) {
		perror("scanf in get_rom_file_name");
		return (char*)NULL;
	}

	return rom_file_name;
}

/* This function opens the rom file with the given file name */
int open_rom_file(const char* rom_file_name)
{
	int rom_fd = -1;
	if(NULL == rom_file_name) {
		printf("rom_file_name is invalid!\n");
		return rom_fd;
	}

	/*Open the rom file*/
	rom_fd = open(rom_file_name, O_RDONLY);
	if(-1 == rom_fd) {
		perror("open in open_rom_file");
	}
	
	return rom_fd;
}

int load_rom_file(int rom_fd, char* buffer)
{
	int result = -1;
	struct stat rom_stat = { 0 };

	if(0 > rom_fd) {
		printf("rom_fd is invalid!\n");
		return rom_fd;
	}

	/*Get size of rom file to check for bounds issues*/
	result = fstat(rom_fd, &rom_stat);
	if(-1 == result) {
		perror("fstat in load_rom_file");
		return result;
	}
	result = -1;

	/*Make sure rom file is small enough*/
	if(3232 < rom_stat.st_size) {
		printf("Given file is %d bytes long! Must be less than 3232 bytes!\n",
			(int)rom_stat.st_size);
		return -1;
	}
	
	/*Read in the rom file into emulated memory*/
	result = (int)read(rom_fd, &buffer[0x200], rom_stat.st_size);
	if(rom_stat.st_size != result) {
		perror("read in load_rom_file");
		return -1;
	}

	return result;
}

