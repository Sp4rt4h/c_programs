/*
Copyright (C) 2018 Matthew Leon

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "main.h"

int main(void)
{
	char* rom_f_name = 0;
	int rom_fd = -1;
	char decision = 0;

	rom_f_name = get_rom_file_name();
	if(NULL == rom_f_name) {
		return -1;
	}

	rom_fd = open_rom_file(rom_f_name);
	if(-1 == rom_fd) {
		return -1;
	}

	load_rom_file(rom_fd, (char*)memory);

	printf("The rom file has been loaded into memory.\n");
	printf("Would you like to execute or disassemble the rom file?\n");
	printf("Type e to execute or d to disassemble\n");

	scanf(" %c", &decision);

	switch(decision) {
		case 'E' :
		case 'e' :
			printf("Now executing\n");
			execute_rom(memory);
			dump_registers();
		break;

		case 'D' :
		case 'd' :
			printf("Now disassembling\n");
		break;

		default :
			printf("You need to type E or D\n");
	}	

	return 0;
}
