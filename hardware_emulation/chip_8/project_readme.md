This project is designed to be a simple emulation for the chip 8 architecture

In order to build the project, just run make all.
Doing so will place the project executable and its object files in the
build directory at c_projects/build.

At the moment, the project just prints out all of the memory values in the
emulated hardware

